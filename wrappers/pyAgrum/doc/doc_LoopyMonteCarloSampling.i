%feature("docstring") gum::LoopySamplingInference<float,gum::MonteCarloSampling>
"
Class used for inferences using a loopy version of Monte Carlo sampling.

LoopyMonteCarloSampling(bn) -> LoopyMonteCarloSampling
    Parameters:
        * **bn** (*pyAgrum.BayesNet*) -- a Bayesian network
"
