%feature("docstring") gum::LoopySamplingInference<float,gum::ImportanceSampling>
"
Class used for inferences using a loopy version of importance sampling.

LoopyImportanceSampling(bn) -> LoopyImportanceSampling
    Parameters:
        * **bn** (*pyAgrum.BayesNet*) -- a Bayesian network
"
