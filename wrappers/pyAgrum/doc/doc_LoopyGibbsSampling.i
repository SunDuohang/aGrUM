%feature("docstring") gum::LoopySamplingInference<float,gum::GibbsSampling>
"
Class used for inferences using a loopy version of Gibbs sampling.

LoopyGibbsSampling(bn) -> LoopyGibbsSampling
    Parameters:
        * **bn** (*pyAgrum.BayesNet*) -- a Bayesian network
"
